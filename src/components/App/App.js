import React, { Component } from 'react';
import 'react-image-gallery/styles/css/image-gallery.css';
import './App.css';
import ImageGallery from 'react-image-gallery';

export default class App extends Component {

  constructor () {
    super();
    const toRight = window.innerWidth < 1000 && window.innerWidth > 768
    this.state = {
      thumbnailPosition: toRight ? 'right' : 'bottom'
    }

    this.images = [
      {
        original: '/assets/slide1.jpg',
        thumbnail: '/assets/slide1.jpg',
        thumbnailClass: 'slider-thumbnail',
        originalClass: 'slider-images'
      },
      {
        original: '/assets/slide2.jpg',
        thumbnail: '/assets/slide2.jpg',
        thumbnailClass: 'slider-thumbnail',
        originalClass: 'slider-images'
      },
      {
        original: '/assets/slide3.jpg',
        thumbnail: '/assets/slide3.jpg',
        thumbnailClass: 'slider-thumbnail',
        originalClass: 'slider-images'
      },
      {
        original: '/assets/slide4.jpg',
        thumbnail: '/assets/slide4.jpg',
        thumbnailClass: 'slider-thumbnail',
        originalClass: 'slider-images'
      }
    ]
  }

  componentDidMount () {
    window.addEventListener("resize", this.onResizeChange.bind(this));
  }

  componentWillUnmount () {
    window.removeEventListener("resize", this.onResizeChange.bind(this));
  }

  onResizeChange () {
    if (window.innerWidth < 1000 && window.innerWidth > 768) this.setState({thumbnailPosition: 'right'})
    else this.setState({thumbnailPosition: 'bottom'})
  }

  render () {
    return (
      <div className="container product-page">
        <header>
          <img src="/assets/logo.png" alt="JOCHEN SCHWEIZER" />
        </header>
        <div className="image-gallery-container">
          <ImageGallery items={this.images} showFullscreenButton={false} showPlayButton={false} showBullets={true} showNav={false} thumbnailPosition={this.state.thumbnailPosition}/>
        </div>
        <div className="title">
          <h1>Fallschirm Tandemsprung</h1>
        </div>
        <div className="description">
          <p>Augenblicke für die Ewigkeit! Fest verbunden mit einem erfahrenen Tandem-Master springst du aus einer Höhe von rund 3.000 bis 4.000 Metern ins Nichts. Nach deinem Exit beschleunigst du wie ein wild gewordener Sportwagen, bis du mit Freifallgeschwindigkeit der Erde entgegen rast. Etwa 50 Sekunden später zieht dein Tandem-Master die Reißleine und du gleitest am Schirm durch die Luft!</p>
          <div className="container-flex  space-between">
            <div className="price">
              <p>199,90 €</p>
              <p>zzgl. Versandkosten</p>
            </div>
            <button>IN DEN WARENKORB</button>
          </div>
        </div>

      </div>
    );
  }
}
